export interface RejectErrorObjectInterface {
    message: string;
    http_status: number | string;
}