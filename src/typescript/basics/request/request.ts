import {RejectErrorObjectInterface} from "basics/request/interface.reject-error-object";

export class XhrRequest {

    private static use_async = true;
    private static method_get = 'GET';

    private static xhr_status_ok = 200;

    static get(url: string, custom_headers: object | boolean = false): Promise<object|RejectErrorObjectInterface> {

        return new Promise((resolve, reject) => {

            const xhr = XhrRequest.createXhrObject();

            XhrRequest.openAsyncGet(xhr, url);

            XhrRequest.setCustomHeaders(xhr, custom_headers);

            xhr.onreadystatechange = () => {

                if (xhr.readyState !== xhr.DONE) {
                    return;
                }

                if (xhr.status === this.xhr_status_ok) {
                    const parsed_json = JSON.parse(xhr.responseText);
                    resolve(parsed_json);
                } else {
                    reject(XhrRequest.createRejectErrorObject(xhr));
                }
            };

            xhr.onerror = () => {
                reject(XhrRequest.createRejectErrorObject(xhr));
            };

            XhrRequest.send(xhr);
        });
    }

    static createRejectErrorObject(xhr: XMLHttpRequest) {
        return <RejectErrorObjectInterface>{
            message: xhr.statusText || 'unknown error, http status ' + xhr.status,
            http_status: xhr.status
        };
    }

    static createXhrObject():XMLHttpRequest {
        return new XMLHttpRequest();
    }

    static openAsyncGet(xhr: XMLHttpRequest, url:string) {
        xhr.open(XhrRequest.method_get, url, XhrRequest.use_async);
    }

    static send(xhr: XMLHttpRequest) {
        xhr.send();
    }

    static setCustomHeaders(xhr: XMLHttpRequest, custom_headers: object | boolean = false):void {

        if(!custom_headers) {
            return;
        }

        const header_names = Object.keys(custom_headers);

        for(let header_name of header_names) {
            const header_value = custom_headers[header_name] || false;

            if(!header_value) {
                continue;
            }

            xhr.setRequestHeader(header_name, header_value);
        }
    }
}