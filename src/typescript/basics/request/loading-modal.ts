export class LoadingModal {

    modal_el: Element;

    constructor(public target_el: Element) {

        this.createModalLayer();

        this.setSize();
    }

    public destroy(): void {

        this.modal_el.classList.add('fade-out');

        this.modal_el.addEventListener('animationend', () => {
            this.removeModalEl()
        }, false);

        this.modal_el.addEventListener('webkitAnimationEnd', () => {
            this.removeModalEl()
        }, false);

        this.modal_el.addEventListener('oanimationend', () => {
            this.removeModalEl()
        }, false);
    }

    private removeModalEl(): void {
        this.modal_el.remove();
    }

    private createModalLayer(): void {

        this.modal_el = document.createElement('div');
        this.modal_el.classList.add('loading-modal');

        this.modal_el.innerHTML = `<i class="icon-spin2 spin loading-modal-icon"></i>`;

        this.target_el.appendChild(this.modal_el);
    }

    private setSize(): void {
        if(this.target_el !== document.body) {
            return;
        }

        const height = window.innerHeight || document.documentElement.clientHeight || document.body.clientHeight;

        this.modal_el.setAttribute("style", `height:${height}px`);
    }
}