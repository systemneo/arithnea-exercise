export class DateHelper {
    static getDateInFutureOrPast(days_in_future:number):Date {

        const today = new Date();

        const future_date = new Date();
        future_date.setDate(today.getDate() + days_in_future);

        return future_date;
    }
}