export interface SwipeOptionsInterface {
    left_callback?: any;
    right_callback?: any;
    down_callback?: any;
    up_callback?: any;
}