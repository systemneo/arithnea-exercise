import {SwipeOptionsInterface} from "basics/touch/interface.swipe-options";

export class Swipe {

    touch_start_x: number;
    touch_start_y: number;

    constructor(public target_el:HTMLElement, public swipe_callbacks:SwipeOptionsInterface, public threshold:number = 1) {

        this.setupSwipeEvents();
    }

    private setupSwipeEvents():void {

        this.addTouchStart();

        this.addTouchMove();
    }

    private addTouchStart():void {

        const handler = (event) => {

            this.touch_start_x = event.touches[0].clientX;
            this.touch_start_y = event.touches[0].clientY;

            this.addTouchMove();

        };

        this.target_el.addEventListener('touchstart', handler);
    }

    private addTouchMove(): void {

        const handler = (event) => {

            const moved_x = this.touch_start_x - event.touches[0].clientX;
            const moved_y = this.touch_start_y - event.touches[0].clientY;

            if(false ===this.isThresholdReached(moved_x, moved_y)) {
                return;
            }

            if(Swipe.isMovedHorizontal(moved_x, moved_y)) {

                if(moved_x < 0) {
                    this.swipeRight();
                } else {
                    this.swipeLeft();
                }

            } else {

                if(moved_y < 0) {
                    this.swipeDown();
                } else {
                    this.swipeUp();
                }
            }

            this.removeTouchMove(handler);
        };

        this.target_el.addEventListener('touchmove', handler);

        this.target_el.addEventListener('touchend', () => {
            this.removeTouchMove(handler);
        });
    }

    private removeTouchMove(handler:any):void {
        this.target_el.removeEventListener('touchmove', handler);
    }

    private isThresholdReached(moved_x:number, moved_y:number):boolean {
        return Math.abs(moved_x) >= this.threshold || Math.abs(moved_y) >= this.threshold;
    }

    private static isMovedHorizontal(moved_x:number, moved_y:number): boolean {
        return Math.abs(moved_x) > Math.abs(moved_y);
    }

    private swipeRight():void {
        if(this.swipe_callbacks.right_callback) {
            this.swipe_callbacks.right_callback();
        }
    }

    private swipeLeft():void {
        if(this.swipe_callbacks.left_callback) {
            this.swipe_callbacks.left_callback();
        }
    }

    private swipeUp():void {
        if(this.swipe_callbacks.up_callback) {
            this.swipe_callbacks.up_callback();
        }
    }

    private swipeDown():void {
        if(this.swipe_callbacks.down_callback) {
            this.swipe_callbacks.down_callback();
        }
    }
}