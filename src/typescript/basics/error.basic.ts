export class BasicError {
    constructor () {
        Error.apply(this, arguments);
    }
}

BasicError.prototype = new Error();