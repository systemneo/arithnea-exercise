export interface ShowTimesCinemaInterface {
    name: string;
    distance: string;
    standard?: Array<string>,
    three_d?: Array<string>,
}