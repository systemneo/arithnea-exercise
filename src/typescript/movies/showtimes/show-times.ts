import {ShowTimesInterface} from "movies/showtimes/interface.show-times";
import {ShowTimesCinemaListInterface} from "movies/showtimes/interface.show-times-cinema-list";
import {ShowTimesCinemaInterface} from "movies/showtimes/interface.show-times-cinema";
import {App} from "app";

export class ShowTimes implements ShowTimesInterface {

    show_times_el: HTMLElement;

    day_class = 'day';
    day_active_class = 'day_active';
    day_pos_left_class = 'day_pos_left';
    day_pos_right_class = 'day_pos_right';
    movie_class = 'movie';

    constructor() {
        this.determineShowtimesElement();

        this.setupSwitchAnimationEvents();
    }

    private setupSwitchAnimationEvents(): void {
        App.registerOnSwitchToDetails(() => {
            this.show_times_el.classList.remove(App.move_out_of_sight_class);
        });

        App.registerOnSwitchToOverviewFinished(() => {
            this.show_times_el.classList.add(App.move_out_of_sight_class);
        });
    }

    public addMovieToDay(date: Date, cinema_list: ShowTimesCinemaListInterface): HTMLElement {

        const date_identifier = ShowTimes.convertDateToIdentifier(date);

        const day_el = document.getElementById(date_identifier);

        const movie_el = document.createElement('div');
        movie_el.classList.add(this.movie_class);

        ShowTimes.createCinemasInTargetEl(
            cinema_list.cinemas || null,
            movie_el
        );

        day_el.appendChild(movie_el);

        return movie_el;
    }

    private static createCinemasInTargetEl(cinemas: Array<ShowTimesCinemaInterface>, target_el: HTMLElement): void {

        if (!Array.isArray(cinemas) || cinemas.length === 0) {
            return;
        }

        for (let cinema of cinemas) {
            const cinema_el = ShowTimes.createCinema(cinema);
            target_el.appendChild(cinema_el);
        }
    }

    private static createCinema(cinema: ShowTimesCinemaInterface): HTMLElement {

        const el = document.createElement('div');
        el.className = 'cinema';

        let inner_html = `
            <div class="headline">
                <h3>${cinema.name}</h3>
                <span class="distance">${cinema.distance}</span>
            </div>
        `;

        inner_html += ShowTimes.createShowTimeList(
            'standard',
            'Standard',
            cinema.standard || null
        );

        inner_html += ShowTimes.createShowTimeList(
            'three-d',
            '3D',
            cinema.three_d || null
        );

        el.innerHTML = inner_html;

        return el;
    }

    private static createShowTimeList(css_class: string, title: string, show_times: Array<string>): string {

        if (!Array.isArray(show_times) || show_times.length === 0) {
            return '';
        }

        const times = '<span>' + show_times.join('</span><span>') + '</span>';

        return `
            <div class="${css_class} showtime_list">
                <h4>${title}</h4>
                <div class="list">
                    ${times}
                </div>
            </div>
         `;
    }

    public addDay(date: Date): HTMLElement {

        const el = document.createElement('div');
        el.className = this.day_class;

        const date_identifier = ShowTimes.convertDateToIdentifier(date);
        el.setAttribute('id', date_identifier);

        this.show_times_el.appendChild(el);

        return el;
    }

    private static convertDateToIdentifier(date: Date) {
        return `day_${date.getFullYear()}_${date.getMonth()}_${date.getDate()}`;
    }

    public activateDay(day_el: HTMLElement): void {
        this.resetActiveDay();

        day_el.classList.add(this.day_active_class);

        this.setInactiveDays(day_el);
    }

    private setInactiveDays(active_day_el: HTMLElement): void {
        this.resetInactiveDays();

        let previous_day_el = active_day_el;

        while (previous_day_el = <HTMLElement> previous_day_el.previousElementSibling || null) {
            previous_day_el.classList.add(this.day_pos_left_class);
        }

        let next_day_el = active_day_el;

        while (next_day_el = <HTMLElement> next_day_el.nextSibling || null) {
            next_day_el.classList.add(this.day_pos_right_class);
        }
    }

    private resetInactiveDays(): void {
        let inactive_days = this.show_times_el.getElementsByClassName(this.day_class) || null;

        if (!inactive_days) {
            return;
        }

        for (let i = 0; i < inactive_days.length; i++) {

            const inactive_day = inactive_days.item(i);

            inactive_day.classList.remove(this.day_pos_left_class);
            inactive_day.classList.remove(this.day_pos_right_class);
        }
    }

    private resetActiveDay(): void {
        const current_active_el = this.show_times_el.getElementsByClassName(this.day_active_class)[0] || false;

        if (!current_active_el) {
            return;
        }

        current_active_el.classList.remove(this.day_active_class);
    }

    private determineShowtimesElement(): void {
        this.show_times_el = document.getElementById('teaser_showtimes')
    }
}