import {ShowTimesCinemaInterface} from "movies/showtimes/interface.show-times-cinema";

export interface ShowTimesCinemaListInterface {
    cinemas: Array<ShowTimesCinemaInterface>;
}