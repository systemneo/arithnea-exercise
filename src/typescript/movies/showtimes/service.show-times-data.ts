import {ShowTimesDataServiceInterface} from "movies/showtimes/interface.show-times-data-service";
import {ShowTimesCinemaListInterface} from "movies/showtimes/interface.show-times-cinema-list";
import {ShowTimesCinemaInterface} from "movies/showtimes/interface.show-times-cinema";

export class ShowTimesDataService implements ShowTimesDataServiceInterface {

    static cinemas = ['CinemaxX', 'Cinestar', 'Schauburg', 'Cinespace', 'City'];
    static standard_show_times = ['11:00am', '13:00pm','15:00pm','17:00pm','19:00pm','21:00pm','23:00pm',];
    static three_d_show_times = ['10:00am', '12:00pm','14:00pm','16:00pm','18:00pm','20:00pm','22:00pm',];

    getShowTimes():ShowTimesCinemaListInterface {

        let cinema_list = ShowTimesDataService.getEmptyCinemasListObj();

        let cinema_random_count = ShowTimesDataService.getRandomInt(
            0,
            ShowTimesDataService.cinemas.length - 1
        );

        for(let i = 0; i <= cinema_random_count; i++) {
            cinema_list.cinemas.push(ShowTimesDataService.createCinema(i));
        }

        return cinema_list;
    }

    private static createCinema(pos:number):ShowTimesCinemaInterface {

        let miles = pos + 1;

        let standards = ShowTimesDataService.createShowTimes(ShowTimesDataService.standard_show_times);
        let three_ds = ShowTimesDataService.createShowTimes(ShowTimesDataService.three_d_show_times);

        let which_should_be_used = ShowTimesDataService.getRandomInt(1,5);

        switch(which_should_be_used) {
            case 1:
                standards = null;
                break;
            case 2:
                three_ds = null;
                break;
            default:
                break;
        }

        return <ShowTimesCinemaInterface> {
            name:  ShowTimesDataService.cinemas[pos],
            distance: `${miles} mi`,
            standard:standards,
            three_d:three_ds,
        };
    }

    private static createShowTimes(show_times:Array<string>):Array<string> {

        let standard_start_pos = ShowTimesDataService.getRandomInt(
            1,
            show_times.length
        );

        return show_times.slice(standard_start_pos - 1);
    }

    private static getEmptyCinemasListObj():ShowTimesCinemaListInterface {
        return <ShowTimesCinemaListInterface>{
            cinemas:[],
        };
    }

    private static getRandomInt(min, max):number {
        return Math.floor(Math.random() * (max - min + 1)) + min;
    }
}