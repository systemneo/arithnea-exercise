import {ShowTimesCinemaListInterface} from "movies/showtimes/interface.show-times-cinema-list";

export interface ShowTimesInterface {
    addDay(date: Date): HTMLElement;

    activateDay(day_el: HTMLElement): void;

    addMovieToDay(date: Date, cinemas?: ShowTimesCinemaListInterface): HTMLElement;
}