import {MovieDataService} from "movies/movie_data_service/movie_data_service";
import {LoadingModal} from "basics/request/loading-modal";
import {App} from "app";
import {NowPlayingMovieItem} from "movies/movie_data_service/interface.now-playing-movie-item";
import {TeaserItemsInterface} from "movies/interface.teaser-items";
import {NowPlayingMovieList} from "movies/movie_data_service/interface.now-playing-movie-list";
import {Swipe} from "basics/touch/swipe";
import {SwipeOptionsInterface} from "basics/touch/interface.swipe-options";
import {ShowTimesInterface} from "movies/showtimes/interface.show-times";
import {DateHelper} from "basics/date-helper";
import {ShowTimesDataServiceInterface} from "movies/showtimes/interface.show-times-data-service";

export class Movies {

    movies: Array<NowPlayingMovieItem>;

    teaser_el: HTMLElement;

    headlines_el: HTMLElement;

    teaser_items: Array<TeaserItemsInterface> = [];

    modal_loading: LoadingModal;

    current_active_item: HTMLElement;

    constructor(public show_times_obj: ShowTimesInterface,
                public show_times_data_service: ShowTimesDataServiceInterface,
                public movie_service: MovieDataService) {

        this.teaser_el = document.getElementById('teaser');

        this.headlines_el = document.getElementById('teaser_headlines');

        this.loadData();
    }

    public getActiveItem(): HTMLElement {
        return this.current_active_item || null;
    }

    private loadData() {

        this.modal_loading = new LoadingModal(document.body);

        this.movie_service.getNowPlayingMovies()
            .then((json: NowPlayingMovieList) => {

                this.movies = json.movies || [];

                this.createTeaserItems();

                this.modal_loading.destroy();
            })
            .catch((error_message: string) => {
                window.alert(`Loading movie data fails with error message: ${error_message}`);
            });
    }

    private createTeaserItems() {

        for (let movie of this.movies) {

            let teaser_item_el = this.createTeaserPicAndRating(movie);

            let headline_item_el = this.createHeadline(movie);

            let day_el_list = this.addMovieShowTimes();

            this.teaser_items.push({
                teaser_el: teaser_item_el,
                headline_el: headline_item_el,
                day_el_list: day_el_list,
                movie: movie,
            });
        }

        this.onTeaserClick(this.teaser_items[0].teaser_el);
    }

    private addMovieShowTimes(): Array<HTMLElement> {

        const days = App.day_item_count;

        let day_el_list = <Array<HTMLElement>> [];

        for (let i = 1; i <= days; i++) {

            const cinemas_mock = this.show_times_data_service.getShowTimes();

            const date = DateHelper.getDateInFutureOrPast(i - 1);

            const day_movie_el = this.show_times_obj.addMovieToDay(date, cinemas_mock);

            day_el_list.push(day_movie_el);
        }

        return day_el_list;
    }

    private setupSwipeLeftRight(headline_item_el: HTMLElement) {

        const threshold = 10;

        const swipe_callbacks = <SwipeOptionsInterface> {
            right_callback: () => {
                Movies.activatePreviousItem(headline_item_el);
            },
            left_callback: () => {
                Movies.activateNextItem(headline_item_el);
            },
            down_callback: () => {
                App.switchToOverviewMode()
            },
        };

        new Swipe(headline_item_el, swipe_callbacks, threshold);
    }

    private static activatePreviousItem(item: HTMLElement): void {

        const previous_item = <HTMLElement> item.previousSibling || false;

        if (!previous_item) {
            return;
        }

        previous_item.click();
    }

    private static activateNextItem(item: HTMLElement): void {

        const next_item = <HTMLElement> item.nextSibling || false;

        if (!next_item) {
            return;
        }

        next_item.click();
    }

    private createHeadline(movie: NowPlayingMovieItem): HTMLElement {

        let el = document.createElement('div');

        el.classList.add('item');

        this.addEvents(el);

        this.setupSwipeLeftRight(el);

        el.setAttribute('id', 'headline_' + movie.id);

        const spans_obj = Movies.convertTitleToSpans(movie.title);

        el.classList.add(`spans_${spans_obj.title_spans_count}`);

        el.innerHTML = `
            <div class="headline_container">${spans_obj.title_spans}</div>
        `;

        this.headlines_el.appendChild(el);

        return el;
    }

    private static convertTitleToSpans(title: string, max_length: number = 20): any {

        const separator = ' ';
        let line = '';
        let lines: Array<string> = [];

        let words = title.split(separator);

        for (let word of words) {

            const tmp_line = line + separator + word;

            const is_line_empty_and_word_longer = (line == '' && word.length >= max_length);
            const is_line_with_new_word_longer = (tmp_line.length > max_length);

            if (is_line_empty_and_word_longer || is_line_with_new_word_longer) {
                lines.push(line.trim());
                line = '';
            }

            line += separator + word;
        }

        if (line !== '') {
            lines.push(line);
        }

        lines = Movies.checkMaxLines(lines, 3);

        return {
            title_spans: '<span>' + lines.join('</span><br /><span>') + '</span>',
            title_spans_count: lines.length,
        };
    }

    private static checkMaxLines(lines: Array<string>, max_lines: number): Array<string> {
        if (lines.length <= max_lines) {
            return lines;
        }

        const last_line_index = max_lines - 1;

        lines = lines.slice(0, max_lines);
        lines[last_line_index] += '...';

        return lines;
    }

    private createTeaserPicAndRating(movie: NowPlayingMovieItem): HTMLElement {

        let el = document.createElement('div');

        el.classList.add('item');

        this.addEvents(el);

        el.setAttribute('id', 'teaser_' + movie.id);
        el.setAttribute('style', `background-image:url(${movie.image});`);

        el.innerHTML = `
            <span class="rating">${movie.vote_average}</span>
        `;

        this.teaser_el.appendChild(el);

        return el;
    }

    private addEvents(el: HTMLElement) {
        el.addEventListener('click', () => {

            if (App.isOverviewMode()) {
                App.switchMode();
            }

            this.onTeaserClick(el);
        });
    }

    private onTeaserClick(clicked_el: HTMLElement): void {

        this.current_active_item = clicked_el;

        const left_class = 'pos_left';
        const left_off_class = 'pos_left_off';
        const right_class = 'pos_right';
        const right_off_class = 'pos_right_off';
        const active_class = 'active';

        let side_class = left_class;
        let side_off_class = left_off_class;

        let no_off_classes = [];

        for (let i = 0; i < this.teaser_items.length; i++) {

            let item = this.teaser_items[i];
            let previous_item = this.teaser_items[(i - 1)] || false;
            let next_item = this.teaser_items[(i + 1)] || false;

            Movies.removeItemClass(item, left_class);
            Movies.removeItemClass(item, left_off_class);
            Movies.removeItemClass(item, right_class);
            Movies.removeItemClass(item, right_off_class);
            Movies.removeItemClass(item, active_class);

            if (item.teaser_el === clicked_el || item.headline_el === clicked_el) {

                Movies.addItemClass(item, active_class);

                if (previous_item) {
                    no_off_classes.push(previous_item);
                }

                if (item) {
                    no_off_classes.push(item);
                }

                if (next_item) {
                    no_off_classes.push(next_item);
                }

                side_class = right_class;
                side_off_class = right_off_class;
                continue;
            }

            Movies.addItemClass(item, side_class);
            Movies.addItemClass(item, side_off_class);
        }

        for (let item of no_off_classes) {
            Movies.removeItemClass(item, left_off_class);
            Movies.removeItemClass(item, right_off_class);
        }
    }

    private static addItemClass(item: TeaserItemsInterface, class_names: string | Array<string>) {

        if (!Array.isArray(class_names)) {
            class_names = [class_names];
        }

        item.teaser_el.classList.add(...class_names);
        item.headline_el.classList.add(...class_names);

        for (let day_el of item.day_el_list) {
            day_el.classList.add(...class_names);
        }
    }

    private static removeItemClass(item: TeaserItemsInterface, class_names: string | Array<string>) {

        if (!Array.isArray(class_names)) {
            class_names = [class_names];
        }

        item.teaser_el.classList.remove(...class_names);
        item.headline_el.classList.remove(...class_names);

        for (let day_el of item.day_el_list) {
            day_el.classList.remove(...class_names);
        }
    }
}