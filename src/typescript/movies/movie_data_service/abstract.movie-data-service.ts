import {MOVIE_DATA_SERVICE_DUMMY_POSTER} from "movies/movie_data_service/const.movie-data-service";
import {MovieDataServiceParams} from "movies/movie_data_service/interface.movie-data-service-params";

export abstract class MovieDataServiceAbstract {

    api_key_name: string;
    api_key: string;

    protected validatePoster(image_url:string | null, base_path?:string) {

        base_path = base_path || '';

        if(image_url && image_url != '') {
            return base_path + image_url;
        }

        return MOVIE_DATA_SERVICE_DUMMY_POSTER;
    }

    protected objectToQueryString(params: MovieDataServiceParams): string {

        const keys = Object.keys(params);
        let pairs: Array<string> = [];

        for (let key of keys) {
            const value = encodeURIComponent(params[key] || '');
            pairs.push(`${key}=${value}`);
        }

        return '?' + pairs.join('&');
    }

    protected addApiKeyToParams(params: MovieDataServiceParams): MovieDataServiceParams {

        params = params || <MovieDataServiceParams> {};

        params[this.api_key_name] = this.api_key;

        return params;
    }
}