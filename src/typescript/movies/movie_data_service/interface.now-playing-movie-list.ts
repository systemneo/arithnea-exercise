import {NowPlayingMovieItem} from "movies/movie_data_service/interface.now-playing-movie-item";

export interface NowPlayingMovieList {
    movies: Array<NowPlayingMovieItem>;
}