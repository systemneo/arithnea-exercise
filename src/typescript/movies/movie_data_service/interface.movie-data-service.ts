export interface MovieDataServiceInterface {

    readonly api_key: string;
    readonly api_key_name: string;
    readonly api_url: string;

    getNowPlayingMovies(): Promise<any>;
}