export interface MovieDataServiceParams {
    [propName: string]: string;
}