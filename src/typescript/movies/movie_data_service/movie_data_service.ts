import {TheMovieDbService} from "movies/movie_data_service/the-movie-db/service.the-movie-db";
import {NoValidTypeError} from "movies/movie_data_service/error.no-valid-type";
import {MovieDataServiceInterface} from "movies/movie_data_service/interface.movie-data-service";

export class MovieDataService {

    static type_the_movie_db = 'the movie db';

    constructor(public data_service: MovieDataServiceInterface) {
        this.data_service = data_service;
    }

    public static create(type:string): MovieDataService {

        type = type || 'no type given';

        const data_service = MovieDataService.factory(type);

        return new MovieDataService(data_service);
    }

    private static factory(type:string): MovieDataServiceInterface {

        type = type || 'no type given';

        switch(type) {
            case MovieDataService.type_the_movie_db:
                return new TheMovieDbService();
            default:
                throw new NoValidTypeError(`The "${type}" is not supported!`);
        }
    }

    public getNowPlayingMovies(): Promise<any> {
        return this.data_service.getNowPlayingMovies();
    }
}