import {TheMovieDbNowPlayingRequestResultInterface} from "movies/movie_data_service/the-movie-db/interface.now-playing-result";

export const GET_NOW_PLAYING_MOCK = <TheMovieDbNowPlayingRequestResultInterface>{
    "results": [
        {
            "vote_count": 834,
            "id": 324852,
            "video": false,
            "vote_average": 6.2,
            "title": "Ich - Einfach unverbesserlich 3",
            "popularity": 114.700001,
            "poster_path": "/qErc42YxuW3uuS7xbiAXOluqSxl.jpg",
            "original_language": "en",
            "original_title": "Despicable Me 3",
            "genre_ids": [878, 12, 16, 35, 10751],
            "backdrop_path": "/puV2PFq42VQPItaygizgag8jrXa.jpg",
            "adult": false,
            "overview": "Diesmal muss Gru feststellen, dass ein Ex-Bösewicht, Ruhestand hin oder her, keinen Frieden bekommt. Und so stolpern der ehemalige Schurke, die drei Mädchen Agnes, Edith und Margo, Grus Frau Lucy und nicht zuletzt die gelben Minions Hals über Kopf in ein neues Abenteuer.",
            "release_date": "2017-07-06"
        }, {
            "vote_count": 447,
            "id": 374720,
            "video": false,
            "vote_average": 7.5,
            "title": "Dunkirk",
            "popularity": 83.575136,
            "poster_path": "/cUqEgoP6kj8ykfNjJx3Tl5zHCcN.jpg",
            "original_language": "en",
            "original_title": "Dunkirk",
            "genre_ids": [28, 18, 36, 53, 10752],
            "backdrop_path": "/fudEG1VUWuOqleXv6NwCExK0VLy.jpg",
            "adult": false,
            "overview": "Zu Beginn von „Dunkirk“ sind Hunderttausende britischer und alliierter Truppen vom Feind eingeschlossen. Am Strand von Dünkirchen haben sie sich bis ans Meer zurückgezogen – und befinden sich in einer ausweglosen Situation.",
            "release_date": "2017-07-27"
        }, {
            "vote_count": 1570,
            "id": 315635,
            "video": false,
            "vote_average": 7.4,
            "title": "Spider-Man: Homecoming",
            "popularity": 82.928987,
            "poster_path": "/dhxkE1PPzVAmb8sH5z3CsD2XmiO.jpg",
            "original_language": "en",
            "original_title": "Spider-Man: Homecoming",
            "genre_ids": [28, 12, 878],
            "backdrop_path": "/fn4n6uOYcB6Uh89nbNPoU2w80RV.jpg",
            "adult": false,
            "overview": "Begeistert von seiner Erfahrung mit den Avengers, kehrt Peter nach Hause zurück, wo er mit seiner Tante May unter dem aufmerksamen Blick seines neuen Mentors Tony Stark lebt. Peter versucht, zurück in seine normale Tagesroutine zu fallen, doch er wird von dem Gedanken abgelenkt, zu beweisen, dass er mehr ist als die freundliche Spinne aus der Nachbarschaft. Doch als Vulture als neuer Bösewicht die Bildfläche betritt, ist alles, was Peter wichtig ist, bedroht…",
            "release_date": "2017-07-13"
        }, {
            "vote_count": 122,
            "id": 339964,
            "video": false,
            "vote_average": 6.8,
            "title": "Valerian - Die Stadt der Tausend Planeten",
            "popularity": 22.409923,
            "poster_path": "/1gAUf4f1748wSukyC3j1OtMRbeA.jpg",
            "original_language": "en",
            "original_title": "Valerian and the City of a Thousand Planets",
            "genre_ids": [12, 878, 28],
            "backdrop_path": "/o6OhxtsgMurL4h68Uqei0aSPMNr.jpg",
            "adult": false,
            "overview": "Im 28. Jahrhundert sorgen der Spezialagent Valerian und seine neue Partnerin Laureline  für Recht und Ordnung in der gesamten Galaxis. Ihr neuester Auftrag, den sie von ihrem raubeinigen Kommandanten bekommen, führt die beiden ungleichen Gesetzeshüter in die gigantische Metropole Alpha, in der unzählige unterschiedliche Spezies aus verschiedensten Teilen des Universums zusammenleben. Mit der Zeit haben sich die Bewohner der Stadt so weit einander angenähert, dass sie all ihr Wissen vereint haben und dies zum Vorteil für alle nutzen. Allerdings kann sich längst nicht jeder mit den herrschenden Verhältnissen anfreunden. Einige schmieden insgeheim Pläne, die nicht nur Alpha, sondern die gesamte Galaxis ins Chaos stürzen könnten. Valerian und Laureline setzen daher alles daran, um das drohende Unheil abzuwenden.",
            "release_date": "2017-07-20"
        }, {
            "vote_count": 3265,
            "id": 297762,
            "video": false,
            "vote_average": 7.1,
            "title": "Wonder Woman",
            "popularity": 19.909295,
            "poster_path": "/3x5hSShQCuOzY91NwwP17MMJDZW.jpg",
            "original_language": "en",
            "original_title": "Wonder Woman",
            "genre_ids": [28, 12, 14],
            "backdrop_path": "/hA5oCgvgCxj5MEWcLpjXXTwEANF.jpg",
            "adult": false,
            "overview": "Vor ihrem Siegeszug als Wonder Woman wurde die Amazonenprinzessin Diana zu einer unüberwindlichen Kriegerin ausgebildet. Sie wuchs in einem abgelegenen Inselparadies auf – erst von einem notgelandeten amerikanischen Piloten erfährt sie von den fürchterlichen Konflikten im Rest der Welt. Darauf verlässt sie ihre Heimat, weil sie überzeugt ist, dass sie der bedrohlichen Situation Herr werden kann. In dem Krieg, der alle Kriege beenden soll, kämpft Diana an der Seite der Menschen, entdeckt allmählich ihr volles Potenzial … und ihre wahre Bestimmung.",
            "release_date": "2017-06-15"
        }, {
            "vote_count": 598,
            "id": 339403,
            "video": false,
            "vote_average": 6.8,
            "title": "Baby Driver",
            "popularity": 17.389051,
            "poster_path": "/dN9LbVNNZFITwfaRjl4tmwGWkRg.jpg",
            "original_language": "en",
            "original_title": "Baby Driver",
            "genre_ids": [28, 80],
            "backdrop_path": "/xWPXlLKSLGUNYzPqxDyhfij7bBi.jpg",
            "adult": false,
            "overview": "Der Film handelt von einem talentierten jungen Fluchtwagenfahrer, der sich ganz auf den Beat seiner persönlichen Playlist verlässt, um der Beste in seinem Job zu werden. Als er das Mädchen seiner Träume trifft, sieht Baby eine Chance, seine kriminelle Karriere an den Nagel zu hängen und einen sauberen Ausstieg zu schaffen. Aber nachdem er gezwungen wird, für einen Gangsterboss zu arbeiten und ein zum Scheitern verurteilter Raubüberfall sein Leben, seine Liebe und seine Freiheit gefährdet, muss er für seine Handlungen geradestehen.",
            "release_date": "2017-07-27"
        }, {
            "vote_count": 820,
            "id": 335988,
            "video": false,
            "vote_average": 6.2,
            "title": "Transformers: The Last Knight",
            "popularity": 12.691357,
            "poster_path": "/26bR30D147OCofpYtJ72S1gLNz2.jpg",
            "original_language": "en",
            "original_title": "Transformers: The Last Knight",
            "genre_ids": [28, 878, 53, 12],
            "backdrop_path": "/Ytv7P13rbwQ3mLpCAY8lBTqI5s.jpg",
            "adult": false,
            "overview": "Bevor Izabella den Erfinder Cade Yeager kennenlernen durfte war ihr Leben recht trostlos, denn sie ist ohne ihre Eltern aufgewachsen. Trotzdem ist sie nicht auf den Kopf gefallen, was Yeager sofort erkennt. Für die beiden beginnt ein Abenteuer voller Transformers...",
            "release_date": "2017-06-22"
        }, {
            "vote_count": 251,
            "id": 400928,
            "video": false,
            "vote_average": 7.6,
            "title": "Begabt – Die Gleichung eines Lebens",
            "popularity": 7.539969,
            "poster_path": "/9Ts7Vc4wLlpI9oox9mkVUE1tBHy.jpg",
            "original_language": "en",
            "original_title": "Gifted",
            "genre_ids": [18],
            "backdrop_path": "/gYtXT7fJNBRZBbBulO11DoRveLT.jpg",
            "adult": false,
            "overview": "Nach dem Tod seiner Schwester fällt es Frank Adler zu, seine junge Nichte Mary großzuziehen. Zwar besitzt er selbst nicht viel und wohnt in Florida in bescheidenen Umständen zur Untermiete bei Roberta, aber das hält ihn nicht davon ab, Mary das Leben eines normalen Mädchens zu ermöglichen. Doch mit sieben Jahren stellt Marys Lehrerin Bonnie in der Schule fest, dass Mary ein Wunderkind ist. Ihre mathematischen Fähigkeiten übersteigen die ihrer Mitmenschen bei weitem.",
            "release_date": "2017-07-13"
        }, {
            "vote_count": 181,
            "id": 341006,
            "video": false,
            "vote_average": 6.2,
            "title": "Das Belko Experiment",
            "popularity": 5.515343,
            "poster_path": "/faJK0dP3S92kQoKtO4LZMjy41kf.jpg",
            "original_language": "en",
            "original_title": "The Belko Experiment",
            "genre_ids": [28, 27, 53],
            "backdrop_path": "/5p7C6TbDozQgKzTnj1tTkqgjzCM.jpg",
            "adult": false,
            "overview": "Eigentlich ist sollte es für die Angestellten der Non-Profit-Organisation Belko Corp. ein ganz normaler Arbeitstag werden. Doch als am Morgen kurzerhand sämtliche einheimischen Bediensteten nach Hause geschickt werden, ist klar, dass etwas anders ist als sonst. Und bald erfahren die Verbleibenden in dem Bürogebäude am Stadtrand Bogotás auch, was: Unbekannte riegeln das Belko-Headquarter komplett ab, kontrollieren das perfide Experiment von außen und dirigieren die 80 Arbeitskollegen über die Lautsprecheranlage. Sie werden vor eine grausame Wahl gestellt: Entweder töten sie sich gegenseitig oder jemand anderes tötet sie alle durch in ihren Körpern versteckte Mini-Sprengsätze. Schnell teilt sich die Bürobelegschaft in zwei Hälften: Die eine, unter Führung des leitenden Angestellten Mike will einfach nur überleben, die andere jagt unter Leitung von Belko-Boss Barry und dessen rechter Hand Wendell fortan die ehemaligen Kollegen. Ein blutiger Überlebenskampf nimmt seinen Lauf...",
            "release_date": "2017-06-15"
        }, {
            "vote_count": 282,
            "id": 283378,
            "video": false,
            "vote_average": 5.9,
            "title": "Fallen - Engelsnacht",
            "popularity": 5.024655,
            "poster_path": "/cJacj8U5QlBNTO3cOcZIm0ELILl.jpg",
            "original_language": "en",
            "original_title": "Fallen",
            "genre_ids": [18, 14, 10749],
            "backdrop_path": "/5sdqc8wYc56J4VvakpBRByegU9j.jpg",
            "adult": false,
            "overview": "Die 17-jährige Lucinda \"Luce\" Price (Addison Timlin) steht im Verdacht, ein Feuer verursacht und damit einen Jungen getötet zu haben. Deshalb wird sie im Zuge ihrer Therapie zur Besserung ins Sword &amp; Cross Internat geschickt. Dort trifft sie auf ihren Mitschüler Daniel Grigori (Jeremy Irvine) und fühlt sich sofort zu ihm hingezogen, ohne zu wissen, dass der mysteriöse Unbekannte in Wahrheit ein Engel ist, auf dem ein Fluch lastet, weil er sie schon seit Tausenden von Jahren liebt.",
            "release_date": "2017-07-13"
        }, {
            "vote_count": 28,
            "id": 440597,
            "video": false,
            "vote_average": 4.9,
            "title": "Wish Upon",
            "popularity": 4.477896,
            "poster_path": "/u0vnocj57vJt5DHoBEqUOD1G4SU.jpg",
            "original_language": "en",
            "original_title": "Wish Upon",
            "genre_ids": [14, 27, 53],
            "backdrop_path": "/l0dvARJQ6xZeVPhxSS5EYibYGBR.jpg",
            "adult": false,
            "overview": "",
            "release_date": "2017-07-27"
        }, {
            "vote_count": 46,
            "id": 340101,
            "video": false,
            "vote_average": 7.3,
            "title": "Ihre beste Stunde",
            "popularity": 3.865033,
            "poster_path": "/9eCPKSWDB7529A2zzxvmNXYooMs.jpg",
            "original_language": "en",
            "original_title": "Their Finest",
            "genre_ids": [10749, 10752, 35, 18],
            "backdrop_path": "/wDp5y5GiQ2PjThnxW3dYSLvLkUk.jpg",
            "adult": false,
            "overview": "London im Zweiten Weltkrieg: Catrin Cole (Gemma Arterton) wird vom British Ministry of Information damit beauftragt, das Drehbuch zu einem Propagandafilm zu überarbeiten und dem Skript eine weibliche Note zu verpassen. Schnell erweckt Catrin mit ihrer lebhaften Art die Aufmerksamkeit des schneidigen Filmemachers Tom Buckley (Sam Claflin), dem sie unter normalen Umständen wohl nie über den Weg gelaufen wäre. Während der Krieg um sie herum tobt, arbeiten Catrin, Buckley und ihre bunt zusammengewürfelte Filmcrew fieberhaft daran, einen Film zu drehen, der das Herz der ganzen Nation erwärmt und dafür sorgt, dass die Menschen wieder Mut schöpfen. Während Catrins Freund, der Künstler Ellis (Jack Huston), ihre neu gefundene Arbeit naserümpfend betrachtet, ist sie bald Feuer und Flamme für den Job, der hinter der Kamera aber nicht nur viel Spaß, sondern auch großes Drama zu bieten hat – ganz wie die Geschichten, die sie für die Leinwand schreibt.",
            "release_date": "2017-07-06"
        }, {
            "vote_count": 173,
            "id": 417678,
            "video": false,
            "vote_average": 7.2,
            "title": "Du neben mir",
            "popularity": 3.492304,
            "poster_path": "/c8W3Go48Mw0GoNXsGK4W9hNO4Vf.jpg",
            "original_language": "en",
            "original_title": "Everything, Everything",
            "genre_ids": [18, 10749],
            "backdrop_path": "/k4o5RdOS6Y1cSfihf1FIoy7DgJe.jpg",
            "adult": false,
            "overview": "",
            "release_date": "2017-06-22"
        }, {
            "vote_count": 78,
            "id": 397422,
            "video": false,
            "vote_average": 5.5,
            "title": "Girls' Night Out",
            "popularity": 3.491226,
            "poster_path": "/sHVW1cYScGygLOPmrpfOA8Vi8SF.jpg",
            "original_language": "en",
            "original_title": "Rough Night",
            "genre_ids": [18, 35],
            "backdrop_path": "/dIeMj3ywYAffE0BpFF3F6nbbzVF.jpg",
            "adult": false,
            "overview": "Nach zehn langen Jahren sind die fünf alten College-Freundinnen Jess, Pippa, Frankie , Alice und Blair endlich wiedervereint: Auf einem wilden Junggesellinnenabschied in Miami lassen sie so richtig die Sau raus! Doch die ausgelassene Party läuft plötzlich aus dem Ruder, als die Clique aus Versehen einen Stripper um die Ecke bringt. Panik bricht aus und für die Frauen beginnt eine wahnsinnige Nacht voller skurriler Eskapaden, die sie nur überstehen werden, wenn alle zusammenhalten…",
            "release_date": "2017-06-29"
        }, {
            "vote_count": 43,
            "id": 346681,
            "video": false,
            "vote_average": 6.2,
            "title": "Wilson",
            "popularity": 3.392503,
            "poster_path": "/3zmRRszN9Ljkut6qgD2UH1cEHpl.jpg",
            "original_language": "en",
            "original_title": "Wilson",
            "genre_ids": [18, 35],
            "backdrop_path": "/2y6yfZSXFiA0xvuK5FOEQzaAxLz.jpg",
            "adult": false,
            "overview": "Wilson ist ein eigenwilliger Einzelgänger, der außer zu seinem Hund zu kaum einem anderen Lebewesen eine Verbindung aufzubauen scheinen kann. Nach dem Tod seines Vaters beschließt der ebenso einsame wie neurotische und irritierend ehrliche Misanthrop, seine Ex-Frau Pippi zu finden, in der Hoffnung, mit ihr einen Neuanfang zu starten, obwohl die beiden eigentlich seit Jahren hoffnungslos zerstritten sind. Doch als Wilson sie findet, muss er zu seiner Überraschung bald feststellen, dass er Vater einer Tochter im Teenager-Alter ist: Claire wurde nach der Geburt zur Adoption freigegeben und deswegen hat Wilson sie nie kennengelernt. Mit allen Mitteln versucht er nun, alle drei als Familie zu einen, um endlich das Idyll zu finden, nach dem er sich schon so lange sehnt...",
            "release_date": "2017-06-29"
        }, {
            "vote_count": 247,
            "id": 376290,
            "video": false,
            "vote_average": 7.1,
            "title": "Die Erfindung der Wahrheit",
            "popularity": 3.337605,
            "poster_path": "/cjnH2ez7z2BLTYpLcf0nsL5DicJ.jpg",
            "original_language": "en",
            "original_title": "Miss Sloane",
            "genre_ids": [18, 53],
            "backdrop_path": "/64xHK6gmw3ZwIpjeSWZ7W686J5l.jpg",
            "adult": false,
            "overview": "Der 2. Zusatzartikel zur Verfassung der Vereinigten Staaten von Amerika, der Einschränkungen im Besitz und Tragen von Waffen seitens der Bundesregierung verbietet, ist seit Jahren politischer Sprengstoff. Auf diesem Schlachtfeld agiert die überaus erfolgreiche politische Strategin Elizabeth Sloane , die auf Grund ihrer Ambition und Skrupellosigkeit eine überaus erfolgreiche Vertreterin ihres Metiers darstellt. Dabei stellt sie die ausbeuterische Welt der Lobbyisten auf beiden Seiten der Debatte im Diskurs über Waffenkontrolle bloß. Ein neues Gesetz, welches strengere Überprüfungen von Waffenbesitzern fordert, gewinnt zunehmend an Zugkraft im Kongress. Sloane wird zur Vorkämpferin der Kampagne, wodurch sie sich einer Vielzahl politischer Gegner gegenübersieht. Mit ihrem allseits bekannten Geschick und von einem eisernen Siegeswillen angetrieben, gefährdet sie ausgerechnet all jene, die ihr am meisten bedeuten. Und auch ihre Karriere ist zunehmend in Gefahr...",
            "release_date": "2017-07-06"
        }, {
            "vote_count": 29,
            "id": 345914,
            "video": false,
            "vote_average": 6.3,
            "title": "Casino Undercover",
            "popularity": 3.06152,
            "poster_path": "/aWbrGS10KpgN5woRsJNqLaoDwR9.jpg",
            "original_language": "en",
            "original_title": "The House",
            "genre_ids": [35],
            "backdrop_path": "/xaGF81kysHmXyak4XSHF3OFPLmR.jpg",
            "adult": false,
            "overview": "Das Ehepaar Scott (Will Ferrell) und Kate Johansen (Amy Poehler) verliert den College-Fund ihrer Tochter Alex. Gemeinsam mit ihrem Nachbarn Frank (Jason Mantzoukas) eröffnen sie in ihrem Keller ein illegales Casino, um das Geld ihrer Tochter zurückzugewinnen und ihr ein Studium doch noch zu ermöglichen.",
            "release_date": "2017-07-06"
        }, {
            "vote_count": 0,
            "id": 422766,
            "video": false,
            "vote_average": 0,
            "title": "Ostwind 3 - Aufbruch nach Ora",
            "popularity": 3.048328,
            "poster_path": null,
            "original_language": "de",
            "original_title": "Ostwind 3 - Aufbruch nach Ora",
            "genre_ids": [10751],
            "backdrop_path": null,
            "adult": false,
            "overview": "Ostwind 3 - Aufbruch nach Ora ist der dritte Teil des erfolgreichen deutschen Jugendbuch-Franchise.",
            "release_date": "2017-07-27"
        }, {
            "vote_count": 0,
            "id": 458171,
            "video": false,
            "vote_average": 0,
            "title": "Sie nannten ihn Spencer",
            "popularity": 2.996979,
            "poster_path": "/fkdFJb8ctne7C5pkoTQLpGM7Y01.jpg",
            "original_language": "de",
            "original_title": "Sie nannten ihn Spencer",
            "genre_ids": [99],
            "backdrop_path": null,
            "adult": false,
            "overview": "Bud Spencer, der mit bürgerlichem Namen eigentlich Carlo Pedersoli hieß, verstarb am 27. Juni 2016. Der ehemalige Profi-Schwimmer, Olympionike und späterer Schauspieler wurde von Millionen Menschen verehrt. Filmemacher Karl-Martin Pold porträtiert ihn und hat dafür unter anderem seinen Schauspielkollegen Terence Hill sowie Bud Spencers Familie und Fans interviewt.",
            "release_date": "2017-07-27"
        }, {
            "vote_count": 73,
            "id": 373569,
            "video": false,
            "vote_average": 4.9,
            "title": "Mädelstrip",
            "popularity": 2.600986,
            "poster_path": "/jBvh2cDVJSzm81SHqBKgy96azxW.jpg",
            "original_language": "en",
            "original_title": "Snatched",
            "genre_ids": [28, 35],
            "backdrop_path": "/6E2TAM2zp2dqgOTLBFHIj8Uu1ip.jpg",
            "adult": false,
            "overview": "Emily (Amy Schumer) wird überraschend von ihrem Freund verlassen. Doch weil sie spontan ist und überhaupt nicht einsieht, den geplanten Trip nach Südamerika ausfallen zu lassen oder alleine zu machen, schnappt sie sich einfach ihre Mutter (Goldie Hawn) als Begleitung. Eine chaotische Reise beginnt, bei der die quirlige Tochter und die vorsichtige Mutter sogar im Knast landen…",
            "release_date": "2017-06-15"
        }
    ],
    "page": 1,
    "total_results": 65,
    "dates": {
        "maximum":
            "2017-08-02", "minimum":
            "2017-06-14"
    },
    "total_pages": 4,
};