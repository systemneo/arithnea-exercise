import {MovieDataServiceAbstract} from "movies/movie_data_service/abstract.movie-data-service";
import {XhrRequest} from "basics/request/request";
import {GET_NOW_PLAYING_MOCK} from "movies/movie_data_service/the-movie-db/mock.get-now-playing";
import {MovieDataServiceInterface} from "movies/movie_data_service/interface.movie-data-service";
import {MovieDataServiceParams} from "movies/movie_data_service/interface.movie-data-service-params";
import {NowPlayingMovieList} from "movies/movie_data_service/interface.now-playing-movie-list";
import {RejectErrorObjectInterface} from "basics/request/interface.reject-error-object";
import {TheMovieDbNowPlayingRequestResultInterface} from "movies/movie_data_service/the-movie-db/interface.now-playing-result";
import {NowPlayingMovieItem} from "movies/movie_data_service/interface.now-playing-movie-item";

export class TheMovieDbService extends MovieDataServiceAbstract implements MovieDataServiceInterface {

    api_key = '01319415788180326b8c992cb3e79517';
    api_key_name = 'api_key';
    api_url = 'https://api.themoviedb.org/3';

    private max_item_count = 10;

    private image_base_url = 'https://image.tmdb.org/t/p/w600/';

    private api_method_path_get_now_playing_movies = '/movie/now_playing';
    private api_method_params_get_now_playing_movies = <MovieDataServiceParams> {
        language: 'de-DE',
        page: '1',
        region: 'DE',
    };

    private api_exceed_limit_http_status_code = 429;

    getNowPlayingMovies(): Promise<NowPlayingMovieList | string> {

        return this.request(
            this.api_method_path_get_now_playing_movies,
            this.api_method_params_get_now_playing_movies,
        );
    }

    private request(method_path: string, params: MovieDataServiceParams): Promise<NowPlayingMovieList | string> {

        params = this.addApiKeyToParams(params);

        const query_string = this.objectToQueryString(params);
        const full_api_path = this.api_url + method_path + query_string;
        const custom_headers = {
            'content-type' : 'application/json;charset=utf-8',
        };

        return new Promise((resolve, reject) => {

            XhrRequest.get(full_api_path, custom_headers)
                .then((json) => {
                    const transformed_data = this.transformResultToStandard(json);
                    resolve(transformed_data);
                })
                .catch((error: RejectErrorObjectInterface) => {
                    switch (error.http_status) {

                        // Fallback: In case the api request limit was exceeded then use mock data for
                        // presentation.
                        case this.api_exceed_limit_http_status_code:
                            const transformed_data = this.transformResultToStandard(GET_NOW_PLAYING_MOCK);
                            resolve(transformed_data);
                            break;
                        default:
                            reject(error.message);
                    }
                });
        });
    }

    private transformResultToStandard(json): NowPlayingMovieList {

        json = <TheMovieDbNowPlayingRequestResultInterface> json;

        let transformed_data = <NowPlayingMovieList> {
            movies: [],
        };

        let ctr = 0;

        for (let result of json.results) {

            ctr++;

            transformed_data.movies.push(<NowPlayingMovieItem>{
                id: result.id,
                vote_average: result.vote_average,
                title: result.title,
                image: this.validatePoster(result.poster_path, this.image_base_url),
            });

            if(ctr >= this.max_item_count) {
                break;
            }
        }

        return transformed_data;
    }
}