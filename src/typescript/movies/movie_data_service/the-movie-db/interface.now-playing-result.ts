import {TheMovieDbNowPlayingRequestResultItemInterface} from "movies/movie_data_service/the-movie-db/interface.now-playing-result-item";

export interface TheMovieDbNowPlayingRequestResultInterface {
    dates?: object,
    page?: number,
    results?: Array<TheMovieDbNowPlayingRequestResultItemInterface>,
    total_pages?: number,
    total_results?: number,
}