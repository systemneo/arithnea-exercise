import {BasicError} from "basics/error.basic";

export class NoValidTypeError extends BasicError {
    constructor (public message: string) {
        super();
    }
}