export interface NowPlayingMovieItem {
    readonly id: number|string;
    readonly vote_average: number;
    readonly title: string;
    readonly image: string;
}