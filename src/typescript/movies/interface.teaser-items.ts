import {NowPlayingMovieItem} from "movies/movie_data_service/interface.now-playing-movie-item";

export interface TeaserItemsInterface {
    teaser_el: HTMLElement;
    headline_el: HTMLElement;
    day_el_list: Array<HTMLElement>;
    movie: NowPlayingMovieItem;
}