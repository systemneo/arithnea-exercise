import {App} from "app";

export class Burger {

    private burger_button: HTMLElement;

    constructor() {
       this.determineElement();

       this.setupEvents();
    }

    private determineElement() {
        this.burger_button = <HTMLElement> document.getElementsByClassName('burger')[0];
    }

    private setupEvents() {
        this.burger_button.addEventListener('click', () => {
            if(App.isOverviewMode()) {
                return;
            }
            App.switchToOverviewMode();
        });
    }
}