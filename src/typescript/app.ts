import {Movies} from "movies/movies";
import {Burger} from "burger/burger";
import {Days} from "days/days";
import {ShowTimes} from "movies/showtimes/show-times";
import {ShowTimesInterface} from "movies/showtimes/interface.show-times";
import {ShowTimesDataService} from "movies/showtimes/service.show-times-data";
import {ShowTimesDataServiceInterface} from "movies/showtimes/interface.show-times-data-service";
import {MovieDataService} from "movies/movie_data_service/movie_data_service";

export class App {

    // This value should be the same as of the $overview_to_detail_animation_duration value in the ./src/scss/vars.scss file!
    static switch_animation_duration_ms = 750;

    static mode_details = 'mode-details';
    static mode_overview = 'mode-overview';

    static move_out_of_sight_class = 'move_out_of_sight_important';

    static burger: Burger;
    static movies: Movies;
    static movie_data_service: MovieDataService;
    static days: Days;
    static show_times: ShowTimesInterface;
    static show_times_data_service: ShowTimesDataServiceInterface;

    static on_switch_to_overview_callbacks: Array<any> = [];
    static on_switch_to_details_callbacks: Array<any> = [];

    static on_switch_to_overview_finished_callbacks: Array<any> = [];
    static on_switch_to_overview_timeout: number;

    static on_switch_to_details_finished_callbacks: Array<any> = [];
    static on_switch_to_details_timeout: number;

    static day_item_count: number;
    static day_item_count_default = 5;

    static scroll_to_top_interval_id:number;

    constructor() {

        App.prepareApp();

        App.initializeDataServices();

        App.initializeComponents();
    }

    private static prepareApp() {
        App.determineDayItemCount();
    }

    private static initializeDataServices() {
        App.show_times_data_service = new ShowTimesDataService();

        App.movie_data_service = MovieDataService.create(MovieDataService.type_the_movie_db);
    }

    private static initializeComponents() {

        App.burger = new Burger();

        App.show_times = new ShowTimes();

        App.movies = new Movies(
            App.show_times,
            App.show_times_data_service,
            App.movie_data_service
        );

        App.days = new Days(App.show_times, App.day_item_count);
    }

    private static determineDayItemCount() {
        App.day_item_count = parseInt(document.body.getAttribute('data-day-item-count')) || App.day_item_count_default;
    }

    public static switchToDetailMode(): void {

        App.onSwitchToDetails();

        App.scrollToTop();

        const body_class_list = document.body.classList;
        body_class_list.remove(App.mode_overview);

        body_class_list.add(App.mode_details);

        App.on_switch_to_details_timeout = window.setTimeout(() => {

            App.onSwitchToDetailsFinished();

        }, App.switch_animation_duration_ms);
    }

    public static switchToOverviewMode() {

        App.onSwitchToOverview();

        const body_class_list = document.body.classList;
        body_class_list.remove(App.mode_details);

        body_class_list.add(App.mode_overview);

        App.on_switch_to_overview_timeout = window.setTimeout(() => {

            App.onSwitchToOverviewFinished();

        }, App.switch_animation_duration_ms);
    }

    public static scrollToTop():void {

        App.stopScrollToTop();

        App.scroll_to_top_interval_id = window.setInterval(() => {

            const window_top = App.getWindowScrollTopPosition();

            if(window_top <= 0) {
                App.stopScrollToTop();
                return;
            }

            let scroll_by = Math.ceil(window_top / 8) * -1;

            window.scrollBy(0, scroll_by);

        }, 40);
    }

    public static getWindowScrollTopPosition():number {

        const doc = document.documentElement;

        return (window.pageYOffset || doc.scrollTop)  - (doc.clientTop || 0);
    }

    public static stopScrollToTop():void {

        if(!App.scroll_to_top_interval_id) {
            return;
        }

        window.clearInterval(App.scroll_to_top_interval_id);

        App.scroll_to_top_interval_id = null;
    }

    public static isOverviewMode(): boolean {
        return !App.isDetailMode();
    }

    public static isDetailMode(): boolean {
        return document.body.classList.contains(App.mode_details);
    }

    public static switchMode() {
        if (App.isDetailMode()) {
            App.switchToOverviewMode();
        } else {
            App.switchToDetailMode();
        }
    }

    public static registerOnSwitchToOverview(callback: any): void {
        App.on_switch_to_overview_callbacks.push(callback);
    }

    public static registerOnSwitchToOverviewFinished(callback: any): void {
        App.on_switch_to_overview_finished_callbacks.push(callback);
    }

    public static onSwitchToOverview(): void {
        App.stopScrollToTop();
        App.executeCallbacksIfExists(App.on_switch_to_overview_callbacks);
    }

    public static onSwitchToOverviewFinished(): void {
        App.stopAllTimeoutsAndIntervals();
        App.executeCallbacksIfExists(App.on_switch_to_overview_finished_callbacks);
    }

    public static stopAllTimeoutsAndIntervals() {
        App.stopScrollToTop();
        App.resetOnSwitchToOverviewTimeout();
        App.resetOnSwitchToDetailTimeout();
    }

    public static resetOnSwitchToOverviewTimeout() {
        if (!App.on_switch_to_overview_timeout) {
            return;
        }

        window.clearTimeout(App.on_switch_to_overview_timeout);

        App.on_switch_to_overview_timeout = null;
    }

    public static registerOnSwitchToDetails(callback: any): void {
        App.on_switch_to_details_callbacks.push(callback);
    }

    public static registerOnSwitchToDetailsFinished(callback: any): void {
        App.on_switch_to_details_finished_callbacks.push(callback);
    }

    public static onSwitchToDetails(): void {
        App.resetOnSwitchToDetailTimeout();
        App.resetOnSwitchToOverviewTimeout();
        App.executeCallbacksIfExists(App.on_switch_to_details_callbacks);
    }

    public static onSwitchToDetailsFinished(): void {
        App.resetOnSwitchToDetailTimeout();
        App.resetOnSwitchToOverviewTimeout();
        App.executeCallbacksIfExists(App.on_switch_to_details_finished_callbacks);
    }

    public static resetOnSwitchToDetailTimeout() {
        if (!App.on_switch_to_details_timeout) {
            return;
        }

        window.clearTimeout(App.on_switch_to_details_timeout);

        App.on_switch_to_details_timeout = null;
    }

    private static executeCallbacksIfExists(callbacks: Array<any>): void {
        if (callbacks.length === 0) {
            return;
        }

        for (let callback of callbacks) {
            callback();
        }
    }
}

const app = new App();