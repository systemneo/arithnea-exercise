import {App} from "app";
import {ShowTimesInterface} from "movies/showtimes/interface.show-times";
import {DayItemsInterface} from "days/interface.day_elements";
import {DateHelper} from "basics/date-helper";

export class Days {

    day_el: HTMLElement;

    day_items: Array<DayItemsInterface>;

    active_class = 'active';

    weekdays = [
        'SUN',
        'MON',
        'TUE',
        'WED',
        'THU',
        'FRI',
        'SAT'
    ];

    constructor(public show_times_obj: ShowTimesInterface, public day_item_count:number) {

        this.determineDayContainer();

        this.createDays();

        this.setupFirstActiveDay();

        this.setupSwitchAnimationEvents();
    }

    private determineDayContainer(): void {
        this.day_el = document.getElementById('days');
    }

    private createDays(): void {

        let days_in_future = 0;

        this.day_items = [];

        do {

            const day_el = document.createElement('div');
            day_el.className = 'item';

            const future_date = DateHelper.getDateInFutureOrPast(days_in_future);
            const future_day = future_date.getDate();
            const future_weekday = this.weekdays[future_date.getDay()];

            day_el.innerHTML = `
<div class="day_containter">
    <span class="day">${future_day}</span>
    <span class="week-day">${future_weekday}</span>
</div>
            `;

            this.day_el.appendChild(day_el);

            const show_times_day_el = this.createShowTimesDay(future_date);

            const day_item = <DayItemsInterface> {
                day_el: day_el,
                show_times_day_el: show_times_day_el,
            };

            this.day_items.push(day_item);

            this.addEvent(day_item);

            days_in_future++;

        } while (days_in_future < this.day_item_count)
    }

    private createShowTimesDay(date: Date): HTMLElement {
        return this.show_times_obj.addDay(date);
    }

    private setupFirstActiveDay(): void {
        const first_item = this.day_items[0];

        this.activateItem(first_item);
    }

    private setupSwitchAnimationEvents(): void {
        App.registerOnSwitchToDetails(() => {
            this.day_el.classList.remove(App.move_out_of_sight_class);
        });

        App.registerOnSwitchToOverviewFinished(() => {
            this.day_el.classList.add(App.move_out_of_sight_class);
        });
    }

    private addEvent(day_item: DayItemsInterface): void {
        day_item.day_el.addEventListener('click', () => {
            this.activateItem(day_item);
        });
    }

    private activateItem(day_item: DayItemsInterface): void {

        this.resetActiveItem();

        day_item.day_el.classList.add('active');

        this.show_times_obj.activateDay(day_item.show_times_day_el);
    }

    private resetActiveItem(): void {
        const current_active_el = this.day_el.getElementsByClassName(this.active_class)[0] || false;

        if (!current_active_el) {
            return;
        }

        current_active_el.classList.remove(this.active_class);
    }

}