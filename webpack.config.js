const ExtractTextPlugin = require('extract-text-webpack-plugin')
const UglifyJSPlugin = require('uglifyjs-webpack-plugin')

const config = {
    entry: [
        './src/typescript/app.ts',
        './src/scss/main.scss'
    ],
    output: {
        filename: './src/js/bundle.js'
    },
    resolve: {
        extensions: ['.tsx', '.ts', '.js', '.json'],
        modules: [
          './src/typescript/',
          './node_modules'
        ]
    },
    module: {
        rules: [
            { // TYPESCRIPT
                test: /\.tsx?$/,
                use: 'ts-loader',
                exclude: /node_modules/
            },

            { // sass / scss loader for webpack
                test: /\.(sass|scss)$/,
                loader: ExtractTextPlugin.extract(['css-loader', 'sass-loader'])
            }
        ]
    },
    plugins: [
        new ExtractTextPlugin({ // define where to save the file
            filename: './src/css/bundle.css',
            allChunks: true
        }),
        new UglifyJSPlugin()
    ],
    watch: false,
    watchOptions: {
        aggregateTimeout: 300,
        poll: 1000,
        ignored: /node_modules/
    }
}

module.exports = config