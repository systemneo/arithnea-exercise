# Evaluation

In this file you will find a summary of techniques, framework and tools that are evaluated to the 
 [Requirements](requirements.md) of the project.

## Techniques

### CSS 3 - @keyframes

[@keyframes](https://developer.mozilla.org/en-US/docs/Web/CSS/CSS_Animations/Using_CSS_animations) is a relative new and
 alternate way to do animations in *HTML* sites.
 
The greatest advantage is an event system which can be covered up via *JavaScript*, e.g. to find out when an animation
 was finished.

##### Compatibility

This technique is support in all current up to date browsers especially the required ones (see [Requirements](requirements.md)).

### SASS

[SASS](http://sass-lang.com/) is a *CSS* extension which was prefered by Aritnhea's *Team Manager Web-Frontend* so it
 would be used.

_NOTE:_ Uses *webpack* to be compiled (see [Setup IDE](setup_ide.md)).

_TIP:_ Useful [functions documentation](http://sass-lang.com/documentation/Sass/Script/Functions.html).

##### Compatibility

This is a tool to generate *CSS* directly when the code is written and available for developers of all OS platforms.

There are no compatibility dependencies to server or clients.

### TypeScript / ECMASCRIPT 6

[TypeScript](https://www.typescriptlang.org/) is a typed superset of JavaScript (EXMASCRIPT 6) and extends it by types, 
interfaces and many more in a standardized way.

_NOTE:_ Uses *webpack* to be compiled (see [Setup IDE](setup_ide.md)).

##### Compatibility

Because it is a typed superset it will be compiled to standard JavaScript (ECMASCRIPT 5.1) so it is compatible to all
current browsers.

## Frameworks

### jQuery

[jQuery](http://jquery.com) is a *JavaScript* framework to manipulate the *HTML DOM*, make *AJAX* calls and more.

The advantage of this framework is the lightweight code size, functionality and spreading (so you can use caching via
 *CDN*s). Furthermore a *slim* version can be used to reduce the code size if only core functionality is used.

##### Compatibility

This framework is supported in all popular browsers especially the required ones (see [Requirements](requirements.md)).

### Angular (excluded)

[Angular](https://angular.io/) is a platform that makes it easy to buold applications with the web. It have a massive
functionality and size but is fast and one of the leading platforms next to *React* or *Vue* for complex single page
applications.

**It was excluded** because it is to heavy for this *little* exercise with focus on handmade *CSS* and *JavaScript*.

## Tools

### JetBrains PhpStorm

[PhpStorm](https://www.jetbrains.com/phpstorm/) is an IDE to develop web based projects (HTML/CSS/JS/PHP).
 
##### Compatibility

Available for all OS platforms.

### Atlassian BitBucket

[BitBucket](https://bitbucket.org) is a versioning repository based on *GIT*.

This tool was choosen because the maintainer simply know this tool and likes it.

##### Compatibility

Available for all OS platforms and browsers.