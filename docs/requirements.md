# Requirements

## Task

Simply: Realize the GIF.

![Exercise demo from Arithnea][exercise_demo]

## Deadline

31.07.2017, 10:00 am

## What needs to be considered?

* Make it smart in HTML/CSS/JS.
* It's possible to use frameworks like *jQuery*, *Angular*, etc. (but consider the relation to the size of the project).
* No SEO or accessibility required.
* No tests required.
* No print version required.
* No technical documentation required.
  * But a summary about the steps, approach and used things in the form of a presentation is required for the job
  interview (eg. as PowerPoint).
* Web-application should run in:
  * Chrome
  * Firefox
  * Edge
  * Safari
  * iOS Safari
* Web-application should be responsive.
  * Focus is the mobile version, the functionality and animations.
  * The tablet/desktop version should use the extended space (but not waste much time on it).
* The main part should be done in *CSS/SASS* for the animations. Only the most necessary should be done in *JavaScript*. 
* Instead of mockup data use a *REST-API* like [TheMovieDB.org](https://www.themoviedb.org/documentation/api/).
* The code should be maintained in a repository, like GIT.

[exercise_demo]: images/Bewerber-Aufgabe.gif "Exercise demo from Arithnea"