# SETUP IDE

## PhpStorm

### SASS and TYPESCRIPT with WEBPACK

#### Initial setup

_NOTE:_ You need *npm* packet manager - which comes with [nodejs](https://nodejs.org/en/) before you can go ahead.

* Open cmd tool (needs administration rights under Windows)
* Switch to the project directory (create one if not exists)
* `npm install -g webpack` (if not already installed)
* `npm init -y` (to initialize the project to *npm*, creates `package.json`)
* `npm install --save-dev webpack typescript ts-loader css-loader node-sass sass-loader extract-text-webpack-plugin uglifyjs-webpack-plugin` 
  (creates `node_modules` directory and all needed files)
* Create `webpack.config.js` in project directory:

```javascript
const ExtractTextPlugin = require('extract-text-webpack-plugin')
const UglifyJSPlugin = require('uglifyjs-webpack-plugin')

const config = {
    entry: [
        './src/typescript/app.ts',
        './src/scss/main.scss'
    ],
    output: {
        filename: './src/js/bundle.js'
    },
    resolve: {
        extensions: ['.tsx', '.ts', '.js', '.json'],
        modules: [
          './src/typescript/',
          './node_modules'
        ]
    },
    module: {
        rules: [
            { // TYPESCRIPT
                test: /\.tsx?$/,
                use: 'ts-loader',
                exclude: /node_modules/
            },

            { // sass / scss loader for webpack
                test: /\.(sass|scss)$/,
                loader: ExtractTextPlugin.extract(['css-loader', 'sass-loader'])
            }
        ]
    },
    plugins: [
        new ExtractTextPlugin({ // define where to save the file
            filename: './src/css/bundle.css',
            allChunks: true
        }),
        new UglifyJSPlugin()
    ],
    watch: false, // Turn true if auto build on file changes.
    watchOptions: {
        aggregateTimeout: 300,
        poll: 1000,
        ignored: /node_modules/
    }
}

module.exports = config
```
   
* Create `tsconfig.json` in project directory:

```json
{
  "compilerOptions": {
    "outDir": "./dist/",
    "sourceMap": true,
    "noImplicitAny": false,
    "module": "commonjs",
    "jsx": "react",
    "allowJs": true,
    "typeRoots": [
      "../node_modules/@types"
    ],
    "baseUrl": "./src/typescript/",
    "target": "es5",
    "lib": [
      "es2015",
      "es5",
      "DOM"
    ]
  },
  "exclude": [
    "node_modules"
  ]
}
```
   
* Change `package.json` in project directory

```json
{
 // ...
 "scripts": {
   // ...
   "build": "webpack"
 }
 // ...
}
```
   
* Add `/node_modules/` to `.gitignore` in the project directory.
* Add `package.json`, `tsconfig.json` and `webpack.config.js` to repository.
* Create the following folders if not exists:

```text
src
|--css
|--js
```
   
* Finally run `webpack` or `npm run build` in the cmd tool.

#### Setup after cloning repository

* Open cmd tool (needs administration rights under Windows)
* Switch to the project directory
* `npm install -g webpack` (if not already installed)
* `npm install`

#### Update project

* Open cmd tool (needs administration rights under Windows)
* Switch to the project directory
* `npm uninstall -g webpack`
* `npm uninstall --save-dev webpack typescript ts-loader css-loader node-sass sass-loader extract-text-webpack-plugin`
* Remove directory `node_modules` from project directory.
* `npm install -g webpack`
* `npm install --save-dev webpack typescript ts-loader css-loader node-sass sass-loader extract-text-webpack-plugin`
* `npm install`

## CODE STYLEGUIDE 

Simply: PSR-2