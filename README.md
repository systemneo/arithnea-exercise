# README

This project is an exercise for a job interview with focus on a smart solution in CSS/JS.

![Exercise demo from Arithnea][exercise_demo]

* [Setup IDE](docs/setup_ide.md)
* [Requirements](docs/requirements.md)
* [Technical Evaluation](docs/evaluation.md)

## Exercise Results

The current results can be shown at [http://systemneo.de/arithnea-exercise/](http://systemneo.de/arithnea-exercise/).

## Responsible Maintainer

* H.-P. Beck

## Technical Contact Persons at Arithnea

* C. Conteh (Team Manager Web-Frontend)

### Other Contact Persons at Arithnea

* E. Schraml (Team Manager eCommerce & DAM)
* R. Mandra (Human Resources Generalist)
* C. Hering (Human Resources - Branch Bremen)
* C. Kohl (Branch Manager Bremen)

[exercise_demo]: docs/images/Bewerber-Aufgabe.gif "Exercise demo from Arithnea"

(c) 2017-2019 systemneo.de